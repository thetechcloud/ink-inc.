#! python
import threading
import time
import os
import Tkinter as tk
print("Welcome to Inc, Incorporated! Here you can buy items to sell, all of which are related to ink!")
keepAlive = True



money = 0
userInput = ""
def beg():
  global money
  money += 1
  print("Begged: +1 Money")
class item:
  def __init__(self):
    self.name = "item"
    self.cost = 0
    self.count = 0
    self.wages = 0
    self.oCost = 0
    
  def buy(self):
    global money
    if money >= self.cost:
      money -= self.cost
      self.count += 1
      print(self.name + " Purchased! Money: "+ str(money))
      self.inflate()
      print(self.getMPS())
    else:
      print("Insufficient money for purchase. Need: " + str(self.cost - money))
      print("Cost: " + str(self.cost))
  def inflate(self):
    self.cost += (self.oCost / 10)
    
  def getCount(self):
    return self.name + "s: " + self.count
  
  def getMPS(self):
    return int(self.wages * self.count)

  def makeBtn(self):
    self.btn = tk.Button(win, text = "Buy " + self.name, command = self.buy)
    self.btn.pack()
class pen(item):
  def __init__(self):
    self.name = "Pen"
    self.cost = 100
    self.wages = 1
    self.count = 16
    self.oCost = 100

class printer(item):
  def __init__(self):
    self.name = "Printer"
    self.cost = 200
    self.wages = 4
    self.count = 0
    self.oCost = 200
  
class squidfarm(item):
  def __init__(self):
    self.name = "Squid Farm"
    self.cost = 400
    self.wages = 16
    self.count = 0
    self.oCost = 400
  
class mntblnk(item):
  def __init__(self):
    self.name = "Mont Blanc"
    self.cost = 800
    self.wages = 64
    self.count = 0
    self.oCost = 800

class earth(item):
  def __init__(self):
    self.name = "Earth"
    self.cost = 1600
    self.wages = 256
    self.count = 0
    self.oCost = 1600
    
class inkplanet(item):
  def __init__(self):
    self.name = "Ink Planet"
    self.cost = 3200
    self.wages = 1025
    self.count = 0
    self.oCost = 3200
    
class upgrade:
  def __init__(self):
    self.boost = 0
    self.name = ''

class game:
  def makeWin(self):
    self.win = tk.Tk()
    self.win.title("Status")
    self.bBeg = tk.Button(self.win, text = "Beg", command = beg)
    self.bPen = tk.Button(self.win, text = "Buy Pen", command = Pen.buy)
    self.bPrinter = tk.Button(self.win, text = "Buy Printer", command = Printer.buy)
    self.bFarm = tk.Button(self.win, text = "Buy Squid Farm", command = Squidfarm.buy)
    self.bMntblnk = tk.Button(self.win, text = "Buy Mont Blanc", command = Mntblnk.buy)
    self.bEarth = tk.Button(self.win, text = "Buy Earth!", command = Earth.buy)
    self.bInkplanet = tk.Button(self.win, text = "Buy Ink Planet", command = Inkplanet.buy)
    self.bMPS = tk.Button(self.win, text = "MPS", command = g.mps)
    self.bStatus = tk.Button(self.win, text = "Money", command = status)
    self.bItems = tk.Button(self.win, text = "Items", command = g.items)
    self.bEnd = tk.Button(self.win, text = "Quit", command = g.end)
    
    self.bBeg.pack()
    self.bPen.pack()
    self.bPrinter.pack()
    self.bFarm.pack()
    self.bMntblnk.pack()
    self.bEarth.pack()
    self.bInkplanet.pack()
    self.bMPS.pack()
    self.bStatus.pack()
    self.bItems.pack()
    self.bEnd.pack()
    
    self.win.mainloop()


  def items(self):
    print("Pens: " + str(Pen.count) + "\nPrinters: " + str(Printer.count) + "\nSquid Farms: " + str(Squidfarm.count) + "\nMont Blancs: " + str(Mntblnk.count) + "\nEarths: " + str(Earth.count) + "\nInk Planets: " + str(Inkplanet.count))

  def mps(self):
    print("Money per second: " + str((Pen.getMPS() + Printer.getMPS() + Squidfarm.getMPS() + Mntblnk.getMPS() + Earth.getMPS() + Inkplanet.getMPS()) / d))
  def end(self):
    keepAlive = False  
    exit()
  

Pen = pen()
Printer = printer()
Squidfarm = squidfarm()
Mntblnk = mntblnk()
Earth = earth()
Inkplanet = inkplanet()
g = game()
d = 2
def profit():
  global money
  
  while keepAlive:
    
    
    time.sleep(1)
    
    money += ((Pen.count * Pen.wages + Printer.count * Printer.wages + Squidfarm.count * Squidfarm.wages + Mntblnk.wages * Mntblnk.count + Earth.count * Earth.wages ) / d)
    if keepAlive != True:
      break



def status():
  print("Current Money: " + str(money))
p = threading.Thread(name='profit', target=profit)
w = threading.Thread(name="UI", target = g.makeWin)
p.start()
w.start()